package com.beesbank.service.accountaccess;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author <a href="mailto:cleclerc@cloudbees.com">Cyrille Le Clerc</a>
 */
public interface AccountRepository extends CrudRepository<Account, Long> {

    Account findByNumber(@Param("number") String number);
}
