package com.beesbank.service.accountaccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private final AccountRepository repository;

    @Autowired
    public DatabaseLoader(AccountRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) throws Exception {
        this.repository.save(new Account("100", 9_000));
        this.repository.save(new Account("101", 90_000));
        this.repository.save(new Account("102", 123));
        this.repository.save(new Account("103", 499));
    }
}